@extends('master')

@section('content')

{{ $status or ' ' }}
<div class="panel panel-info">
<div class="panel-heading">
Data Penulis

<div class="pull-right">
<a href="{{ url('admin/tambah')}}"class="btn btn-success btn-xxl">Tambah Data</a>
</div>
</div>
<div class="panel-body">
<table class="table">
<tr>
<td>Nama</td>
<td>No Telepon</td>
<td>Email</td>
<td>Alamat</td>
</tr>

@foreach($Admin as $Admin)

<tr>
<td >{{ $Admin->nama }}</td>
<td >{{ $Admin->notelp}}</td>
<td >{{ $Admin->email }}</td>
<td >{{ $Admin->alamat}}</td>
<td >

<a href="{{url('admin/edit/'.$Admin->id)}}"class="btn btn-success btn-xxl">Edit</a>
<a href="{{url('admin/hapus/'.$Admin->id)}}"class="btn btn-danger btn-xxl">Hapus</a>
</td>
</tr>

@endforeach

</table>
</div>
</div>

@endsection