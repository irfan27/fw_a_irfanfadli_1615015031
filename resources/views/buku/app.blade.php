@extends('master')
@section('content')
{{ $status or ' ' }}
<div class="panel panel-default">
<div class="panel-heading">
<strong>Data Buku</strong>
<div class="pull-right">
<a href="{{url('tambah/buku')}}"class="btn btn-success btn-xxl"> Tambah Data</a>
</div>
<div class="penel-body">
<table class="table">
<tr>
<td> Judul </td>
<td> Kategori </td>
<td> Penerbit </td>
<td> Penulis </td>
<td> Aksi </td>
</tr>
@foreach($buku as $Buku)
<tr>
<td>{{ $Buku->judul}}</td>
<td>{{ $Buku->kategori_id->deskripsi or 'kosong'}}</td>
<td>{{ $Buku->penerbit }}</td>
<td>{{ $Buku->penulis->first()->nama or 'kosong'}}</td>
<td>
<a href="{{url('buku/edit/'.$Buku->id)}}"class="btn btn-success btn-xxl">Edit</a>
<a href="{{url('buku/hapus/'.$Buku->id)}}"class="btn btn-danger btn-xxl">Hapus</a>
</td>
</tr>
@endforeach
</table>
</div>
</div>
</div>
@endsection