<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pembeli extends Model
{
    protected $table = 'pembeli';
	protected $fillable = [

		'nama','notlp','email','alamat'

	];
	
	public function Pengguna(){
    	return $this->belongsTo(Pengguna::class);
    }

    public function Buku(){
			
			return $this->belongsToMany(Buku::class);
	}
	public function Buku_Pembeli(){

			return $this->hasOne(Buku_Pembeli::class);
	}
	Public function getUsernameAttribute(){
	return $this->pengguna->username;
	}

	Public function getPasswordAttribute(){
	return $this->pengguna->password;
	}
}
