<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Admin;

class AdminController extends Controller
{
    
    public function awal(){


    $Admin = Admin::all();
    return view('Admin.app',compact('Admin'));
}



  public function tambah(){

      return view ("admin.tambah");
  }

  public function simpan(Request $input){

    $this->validate($input,[
     'nama' => 'required',
     'notelp' => 'required',
     'email' => 'required',
     'alamat' => 'required',
]);

    $Admin = new Admin();
  $Admin->nama = $input->nama;
  $Admin->notelp = $input->notelp;
  $Admin->email = $input->email;
  $Admin->alamat = $input->alamat;
  $Admin->save();
  return redirect ('admin');
  }
   

   public function edit($id){

        $Admin = Admin::find($id);
        return view('Admin.edit')->with(array('Admin'=>$Admin));


   }

   public function update(Request $input, $id){

    $this->validate($input,[
     'nama' => 'required',
     'notelp' => 'required',
     'email' => 'required',
     'alamat' => 'required',
]);

      $Admin = Admin::find($id);
      $Admin->nama = $input->nama;
      $Admin->notelp = $input->notelp;
      $Admin->email = $input->email;
      $Admin->alamat = $input->alamat;
      $status = $Admin->save();
      return redirect ('admin')->with(['status'=>$status]);
   }

   public function hapus($id){

      $Admin = Admin::find($id);
      $Admin ->delete();
      return redirect('admin'); 
   }





    //
}
